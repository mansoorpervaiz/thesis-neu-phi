text_extraction 2

The Royal College of Physicians (2004). Multiple Sclerosis. National clinical guideline for diagnosis and management in primary and secondary care. Salisbury, Wiltshire: Sarum ColourView Group. ISBN 1-86016-182-0.Free full text (2004-08-13). Retrieved on 2007-10-01.
-Spasticity is characterised by increased stiffness and slowness in limb movement, the development of certain postures, an association with weakness of voluntary muscle power, and with involuntary and sometimes painful spasms of limbs.[33] 


Koch M, Mostert J, Heersema D, De Keyser J (2007). "Tremor in multiple sclerosis". J. Neurol. 254 (2): 133–45. doi:10.1007/s00415-006-0296-7. PMC 1915650Freely accessible. PMID 17318714.
-Tremor and ataxia are frequent in MS and present in 25 to 60% of patients. They can be very disabling and embarrassing, and are difficult to manage.[80] T

Office of Communications and Public Liaison (February 2002). "Traumatic brain injury: Hope through research". NIH Publication No. 02-2478. National Institute of Neurological Disorders and Stroke, National Institutes of Health. Retrieved 2008-08-17. Many patients with mild to moderate head injuries who experience cognitive deficits become easily confused or distracted and have problems with concentration and attention. They also have problems with higher level, so-called executive functions, such as planning, organizing, abstract reasoning, problem solving, and making judgments, which may make it difficult to resume pre-injury work-related activities. Recovery from cognitive deficits is greatest within the first 6 months after the injury and more gradual after that.
-Movement disorders that may develop after TBI include tremor, ataxia (uncoordinated muscle movements), myoclonus (shock-like contractions of muscles), and loss of movement range and control (in particular with a loss of movement repertoire).[84]


"NINDS Traumatic Brain Injury Information Page". National Institute of Neurological Disorders and Stroke. September 15, 2008. Retrieved 2008-10-27
-weakness or numbness in the limbs, loss of coordination, confusion, restlessness, or agitation.[34] 